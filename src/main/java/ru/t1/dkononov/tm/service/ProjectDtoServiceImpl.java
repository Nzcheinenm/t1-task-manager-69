package ru.t1.dkononov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.api.service.UserService;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.entity.model.Project;
import ru.t1.dkononov.tm.entity.model.User;
import ru.t1.dkononov.tm.mapper.ProjectMapper;
import ru.t1.dkononov.tm.mapper.UserMapper;
import ru.t1.dkononov.tm.repository.ProjectRepository;
import ru.t1.dkononov.tm.utils.UserUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectDtoServiceImpl implements ProjectDtoService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserService userService;


    @Override
    public List<ProjectDto> findAll(final String userId) {
        return projectRepository.findAllByUserId(userId)
                .stream()
                .map(ProjectMapper::projectToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProjectDto save(final ProjectDto projectDto) {
        final Project project = ProjectMapper.dtoToProject(projectDto);
        final User user = UserMapper.dtoToUser(userService.findById(projectDto.getUserId()));
        project.setUser(user);
        return ProjectMapper.projectToDto(projectRepository.save(project));
    }

    @Override
    public ProjectDto findById(final String id) {
        final Optional<Project> projectDtoOptional = projectRepository.findById(id);
        return projectDtoOptional.map(ProjectMapper::projectToDto).orElse(null);
    }

    @Override
    public boolean exsistsById(final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public ProjectDto save(final String userId) {
        final ProjectDto projectDto = new ProjectDto("New name " + LocalDateTime.now().toString());
        projectDto.setUserId(userId);
        final Project project = ProjectMapper.dtoToProject(projectDto);
        final User user = UserMapper.dtoToUser(userService.findById(projectDto.getUserId()));
        project.setUser(user);
        return ProjectMapper.projectToDto(projectRepository.save(project));
    }

    @Override
    public ProjectDto findByUserIdAndId(final String userId, final String id) {
        final Optional<Project> projectDtoOptional = projectRepository.findByUserIdAndId(userId, id);
        return projectDtoOptional.map(ProjectMapper::projectToDto).orElse(null);
    }

    @Override
    public boolean exsistsByUserIdAndId(final String userId, final String id) {
        return projectRepository.findByUserIdAndId(UserUtil.getUserId(), id).isPresent();
    }

    @Override
    public void deleteByUserIdAndId(final String userId, final String id) {
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    public void deleteById(final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    public void delete(final ProjectDto project) {
        projectRepository.delete(ProjectMapper.dtoToProject(project));
    }

    @Override
    public void deleteAll(final List<ProjectDto> projectsDto) {
        List<Project> projects = new ArrayList<>();
        projects = projectsDto
                .stream()
                .map(ProjectMapper::dtoToProject)
                .collect(Collectors.toList());
        projectRepository.deleteAll(projects);
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

}
