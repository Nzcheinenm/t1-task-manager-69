package ru.t1.dkononov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.CustomUser;

@Controller
public class TasksController {

    @Autowired
    private TaskDtoService taskDtoService;

    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("task-list", "tasks", taskDtoService.findAll(user.getUserId()));
    }

}
