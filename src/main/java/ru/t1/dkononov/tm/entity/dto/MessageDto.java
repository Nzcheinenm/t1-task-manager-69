package ru.t1.dkononov.tm.entity.dto;

public class MessageDto {

    private String value;

    public MessageDto(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
