package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.api.endpoint.TaskEndpoint;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.utils.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDtoService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    @Secured({"ROLE_ADMINISTRATOR","ROLE_USER"})
    public List<TaskDto> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDto save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    ) {
        task.setUserId(UserUtil.getUserId());
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/exsitsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.exsistsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public long count() {
        return taskService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    ) {
        task.setUserId(UserUtil.getUserId());
        taskService.delete(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDto> tasks
    ) {
        taskService.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void clear() {
        taskService.clear();
    }

}
